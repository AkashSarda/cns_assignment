#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>

char increment(char ch, int k, char st, char end){
	if(ch + k > end){
		ch = ch + k - end + st;
	}else{
		ch += k;
	}

	return ch;
}


int main(int argc, char *argv[]){
	int fd, fd2, k;
	scanf("%d", &k);
	fd = open(argv[1], O_RDONLY);
	fd2 = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
	char ch;

	while(read(fd, &ch, 1)){
		if(ch >= 'A' && ch < 'z'){
			if(ch >= 'A' && ch <= 'Z'){
				ch = increment(ch, k, 'A', 'Z');
			}else{
				ch = increment(ch, k, 'a', 'z');
			}				
		}

		write(fd2, &ch, 1);
		printf("%c", ch);
	}

	close(fd);
	close(fd2);
}
